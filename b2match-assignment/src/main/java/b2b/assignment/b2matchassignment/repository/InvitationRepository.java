package b2b.assignment.b2matchassignment.repository;

import b2b.assignment.b2matchassignment.model.Invitation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvitationRepository extends JpaRepository<Invitation,Long> {
}
