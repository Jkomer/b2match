package b2b.assignment.b2matchassignment.repository;

import b2b.assignment.b2matchassignment.model.Organisation;
import b2b.assignment.b2matchassignment.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrganisationRepository extends JpaRepository<Organisation, Long> {

    boolean existsByName(String name);
    Optional<Organisation> findByName(String name);
}
