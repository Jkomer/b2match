package b2b.assignment.b2matchassignment.repository;

import b2b.assignment.b2matchassignment.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository  extends JpaRepository<Event, Long> {

    boolean existsByName(String name);
    Optional<Event> findByName(String name);
}
