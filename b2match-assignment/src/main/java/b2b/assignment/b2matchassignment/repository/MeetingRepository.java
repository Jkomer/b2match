package b2b.assignment.b2matchassignment.repository;

import b2b.assignment.b2matchassignment.model.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeetingRepository extends JpaRepository<Meeting, Long> {
}
