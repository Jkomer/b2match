package b2b.assignment.b2matchassignment.model.dto;

import b2b.assignment.b2matchassignment.model.Organisation;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
public class UserDTO {
    private String email;
    private String firstName;
    private String lastName;
    private Long organisationId;
}
