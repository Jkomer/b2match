package b2b.assignment.b2matchassignment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Meeting {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "meetingId")
    private Long meetingId;

    @Column
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime startOfMeeting;

    @Column
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime endOfMeeting;

    @Column
    private String location;

    @Column(nullable = false)
    private String status;

    @ManyToOne
    @JoinColumn(name = "eventId", nullable = false)
    private Event event;

    @Column
    private String organiserEmail;


    @Column
    @ManyToMany(mappedBy = "meetings")
    private List<User> invitees;

    @Column
    @OneToMany(mappedBy = "meeting", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Invitation> meetingInvitations;


    public Meeting(LocalDateTime startOfMeeting, LocalDateTime endOfMeeting, String location, String status, Event event, String organiserEmail) {
        this.startOfMeeting = startOfMeeting;
        this.location = location;
        this.status = status;
        this.event = event;
        this.organiserEmail = organiserEmail;
        this.endOfMeeting = endOfMeeting;
    }
}
