package b2b.assignment.b2matchassignment.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "invitationId")
    private Long invitationId;


    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User invitee;

    @ManyToOne
    @JoinColumn(name = "meetingId", nullable = false)
    private Meeting meeting;


    @Column
    private String status;


    public Invitation(User user, Meeting meeting) {
        this.invitee = user;
        this.meeting = meeting;
    }
}
