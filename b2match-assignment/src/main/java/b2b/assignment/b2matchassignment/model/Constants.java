package b2b.assignment.b2matchassignment.model;

public class Constants {

    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_SCHEDULED = "SCHEDULED";
    public static final String STATUS_CANCELED = "CANCELED";
    public static final String STATUS_ONGOING = "ONGOING";
    public static final String STATUS_FINISHED = "FINISHED";
    public static final String STATUS_ACCEPTED = "ACCEPTED";
    public static final String STATUS_REJECTED = "REJECTED";

}
