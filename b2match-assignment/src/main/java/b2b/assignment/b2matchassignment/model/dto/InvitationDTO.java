package b2b.assignment.b2matchassignment.model.dto;

import b2b.assignment.b2matchassignment.model.Invitation;
import lombok.Data;

@Data
public class InvitationDTO {
    private String email;
    private String status;

    public InvitationDTO(Invitation invitation) {
        this.email = invitation.getInvitee().getEmail();
        this.status = invitation.getStatus();
    }
}
