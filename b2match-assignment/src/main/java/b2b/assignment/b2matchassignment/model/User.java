package b2b.assignment.b2matchassignment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "userId")
    private Long userId;

    @Column
    private String email;

    @Column
    private String firstName;
    @Column
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "organisationId", nullable = false)
    @JsonIgnore
    private Organisation organisation;

    @ManyToMany(mappedBy = "users")
    private List<Event> events;

    @ManyToMany( fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.REFRESH,
                    CascadeType.PERSIST
            })
    @JoinTable(name = "user_meetings",
            joinColumns = @JoinColumn(name = "userId"),
            inverseJoinColumns = @JoinColumn(name = "meetingId"))
    private List<Meeting> meetings;

    @Column
    @OneToMany(mappedBy = "invitee", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Invitation> invitations;

    public User(String email, String firstName, String lastName, Organisation organisation) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.organisation = organisation;
    }
}
