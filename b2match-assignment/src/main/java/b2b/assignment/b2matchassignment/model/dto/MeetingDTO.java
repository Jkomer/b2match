package b2b.assignment.b2matchassignment.model.dto;

import b2b.assignment.b2matchassignment.model.Meeting;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
public class MeetingDTO {
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime startOfMeeting;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime endOfMeeting;
    private Long eventId;
    private String location;
    private String status;
    private String organiserEmail;

    public MeetingDTO(Meeting meeting) {
        this.startOfMeeting = meeting.getStartOfMeeting();
        this.endOfMeeting = meeting.getEndOfMeeting();
        this.eventId = meeting.getEvent().getEventId();
        this.location = meeting.getLocation();
        this.status = meeting.getStatus();
        this.organiserEmail = meeting.getOrganiserEmail();
    }
}
