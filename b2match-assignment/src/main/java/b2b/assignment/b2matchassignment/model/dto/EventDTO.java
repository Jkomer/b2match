package b2b.assignment.b2matchassignment.model.dto;

import b2b.assignment.b2matchassignment.model.Event;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.sql.Date;

@Data
@NoArgsConstructor
public class EventDTO {
    private String name;
    private String description;
    private Date eventStart;
    private Date eventEnd;

    public EventDTO(Event event) {
        this.name = event.getName();
        this.description = event.getDescription();
        this.eventEnd = event.getEventEnd();
        this.eventStart = event.getEventStart();
    }
}
