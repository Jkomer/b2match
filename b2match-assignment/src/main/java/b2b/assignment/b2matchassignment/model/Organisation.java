package b2b.assignment.b2matchassignment.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Organisation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "organisationId")
    private Long organisationId;

    @Column
    private String name;

    @Column
    @OneToMany(mappedBy = "organisation", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<User> users;

    public Organisation(String name) {
        this.name = name;
    }
}
