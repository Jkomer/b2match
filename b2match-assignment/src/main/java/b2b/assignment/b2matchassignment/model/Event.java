package b2b.assignment.b2matchassignment.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "eventId")
    private Long eventId;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Date eventStart;

    @Column
    private Date eventEnd;

    @Column
    @ManyToMany(
            fetch = FetchType.LAZY,
            cascade = {
                            CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.REFRESH,
                            CascadeType.PERSIST
                    })
    @JoinTable(name = "user_events",
            joinColumns = @JoinColumn(name = "eventId"),
            inverseJoinColumns = @JoinColumn(name = "userId"))
    private List<User> users;


    @Column
    @OneToMany(mappedBy = "event", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Meeting> meetings;

    public Event(String name, String description, Date eventStart, Date eventEnd) {
        this.name = name;
        this.description = description;
        this.eventStart = eventStart;
        this.eventEnd = eventEnd;
    }


}
