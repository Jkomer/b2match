package b2b.assignment.b2matchassignment.model.dto;

import lombok.Data;

@Data
public class OrganisationDTO {
    private String name;
}
