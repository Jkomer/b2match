package b2b.assignment.b2matchassignment.controller;

import b2b.assignment.b2matchassignment.model.Event;
import b2b.assignment.b2matchassignment.model.dto.EventDTO;
import b2b.assignment.b2matchassignment.service.UserService;
import b2b.assignment.b2matchassignment.service.impl.EventServiceImpl;
import b2b.assignment.b2matchassignment.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/event")
public class EventController {

    private EventServiceImpl eventService;
    private UserServiceImpl userService;

    @Autowired
    public EventController(EventServiceImpl eventService, UserServiceImpl userService) {
        this.eventService = eventService;
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity createEvent(@RequestBody  EventDTO dto) {
        Long eventId = null;
        try {
             eventId = this.eventService.createEvent(dto.getName(),dto.getDescription(),dto.getEventStart(),dto.getEventEnd());
        } catch (EntityExistsException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ok(eventId);
    }

    @GetMapping
    public ResponseEntity findTopN(@RequestParam(name = "top", defaultValue = "5") int top) {
        List<EventDTO> events = this.eventService.findTopEvents(top).stream()
                .map(EventDTO::new).collect(Collectors.toList());

        return ok(events);
    }
}
