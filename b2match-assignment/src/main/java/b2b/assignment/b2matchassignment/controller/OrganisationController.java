package b2b.assignment.b2matchassignment.controller;

import b2b.assignment.b2matchassignment.model.Organisation;
import b2b.assignment.b2matchassignment.model.dto.OrganisationDTO;
import b2b.assignment.b2matchassignment.service.impl.OrganisationServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/organisation")
@Slf4j
public class OrganisationController {

    private OrganisationServiceImpl organisationService;

    @Autowired
    public OrganisationController(OrganisationServiceImpl organisationService) {
        this.organisationService = organisationService;
    }

    @GetMapping
    public ResponseEntity getOrganisationByName(@RequestParam(value = "name") String name) {
        Organisation organisation = null;
        try {
            organisation = this.organisationService.getOrganisationByName(name);
        } catch (EntityNotFoundException enfe) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ok(organisation);
    }

    @PostMapping
    public ResponseEntity createOrganisation(@RequestBody OrganisationDTO organisationDTO) {
        Organisation organisation = null;
        try {
           organisation = this.organisationService.createOrganisation(organisationDTO);
        } catch (EntityExistsException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ok(organisation.getOrganisationId());
    }

    @DeleteMapping
    public ResponseEntity deleteOrganisation(@RequestBody OrganisationDTO organisationDTO) {
        try {
            this.organisationService.deleteOrganisation(organisationDTO);
        } catch (EntityExistsException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
