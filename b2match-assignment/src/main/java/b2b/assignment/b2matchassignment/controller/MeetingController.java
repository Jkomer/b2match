package b2b.assignment.b2matchassignment.controller;

import b2b.assignment.b2matchassignment.model.dto.MeetingDTO;
import b2b.assignment.b2matchassignment.service.MeetingService;
import b2b.assignment.b2matchassignment.service.impl.MeetingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/meeting")
public class MeetingController {

    private MeetingServiceImpl meetingService;

    @Autowired
    public MeetingController(MeetingServiceImpl meetingService) {
        this.meetingService = meetingService;
    }

    @PostMapping
    public ResponseEntity createMeeting(@RequestBody MeetingDTO dto) {
        Long meetingId = this.meetingService.createMeeting(dto);
        return ok(meetingId);
    }

}
