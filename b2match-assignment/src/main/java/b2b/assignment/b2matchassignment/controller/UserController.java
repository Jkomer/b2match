package b2b.assignment.b2matchassignment.controller;

import b2b.assignment.b2matchassignment.model.Invitation;
import b2b.assignment.b2matchassignment.model.Meeting;
import b2b.assignment.b2matchassignment.model.dto.InvitationDTO;
import b2b.assignment.b2matchassignment.model.dto.MeetingDTO;
import b2b.assignment.b2matchassignment.model.dto.UserDTO;
import b2b.assignment.b2matchassignment.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private UserServiceImpl userService;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity createUser(@RequestBody UserDTO dto) {
        Long userId = null;
        try {
             userId = this.userService.createUser(dto);
        } catch (EntityExistsException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ok(userId);
    }

    @DeleteMapping
    public ResponseEntity deleteUser(@RequestBody UserDTO dto) {
        try {
            this.userService.deleteUser(dto.getEmail());
        } catch (EntityNotFoundException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/register")
    public ResponseEntity registerUserToEvent(@RequestBody UserDTO dto,
                                              @RequestParam(name = "eventId") Long eventId) {
        try {
            this.userService.registerToEvent(dto, eventId);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/unregister")
    public ResponseEntity unRegisterUserToEvent(@RequestBody UserDTO dto,
                                                @RequestParam(name = "eventId") Long eventId) {
        try {
            this.userService.unRegisterFromEvent(dto, eventId);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("{userId}/meeting")
    public ResponseEntity getUserMeetings(@PathVariable Long userId) {
        List<MeetingDTO> meetings = null;
        try {
          meetings = this.userService.getUserMeetings(userId);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ok(meetings);
    }

    @GetMapping("{userId}/invitations")
    public ResponseEntity getUserInvitations(@PathVariable Long userId) {
        List<InvitationDTO> invitations = null;
        try {
            invitations = this.userService.getUserInvitations(userId);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ok(invitations);
    }


}
