package b2b.assignment.b2matchassignment.controller;

import b2b.assignment.b2matchassignment.model.dto.InvitationDTO;
import b2b.assignment.b2matchassignment.service.impl.InvitationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/v1/invitation")
public class InvitationController {

    private InvitationServiceImpl invitationService;

    @Autowired
    public InvitationController(InvitationServiceImpl invitationService) {
        this.invitationService = invitationService;
    }

    @PostMapping
    public ResponseEntity sendInvitation(@RequestBody  InvitationDTO dto,
                                         @RequestParam(name = "meetingId") Long meetingId) {
        Long invitationId = this.invitationService.sendInvitation(dto, meetingId);
        return ok(invitationId);
    }

    @PutMapping
    public ResponseEntity handleInvitation(@RequestBody InvitationDTO dto,
                                           @RequestParam(name = "invitationId") Long invitationId) {
        this.invitationService.handleInvitation(dto,invitationId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
