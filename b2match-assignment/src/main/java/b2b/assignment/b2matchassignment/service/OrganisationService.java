package b2b.assignment.b2matchassignment.service;

import b2b.assignment.b2matchassignment.model.Organisation;
import b2b.assignment.b2matchassignment.model.User;
import b2b.assignment.b2matchassignment.model.dto.OrganisationDTO;

public interface OrganisationService {

    Organisation createOrganisation(OrganisationDTO dto);
    Organisation getOrganisationByName(String name);
    void deleteOrganisation(OrganisationDTO dto);
}
