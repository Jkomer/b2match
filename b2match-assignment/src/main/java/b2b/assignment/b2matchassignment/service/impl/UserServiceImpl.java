package b2b.assignment.b2matchassignment.service.impl;

import b2b.assignment.b2matchassignment.model.*;
import b2b.assignment.b2matchassignment.model.dto.InvitationDTO;
import b2b.assignment.b2matchassignment.model.dto.MeetingDTO;
import b2b.assignment.b2matchassignment.model.dto.UserDTO;
import b2b.assignment.b2matchassignment.repository.EventRepository;
import b2b.assignment.b2matchassignment.repository.OrganisationRepository;
import b2b.assignment.b2matchassignment.repository.UserRepository;
import b2b.assignment.b2matchassignment.service.UserService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    EventRepository eventRepository;
    OrganisationRepository organisationRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           EventRepository eventRepository,
                           OrganisationRepository organisationRepository) {

        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
        this.organisationRepository = organisationRepository;
    }


    @Override
    public Long createUser(UserDTO dto) {
        Organisation organisation = this.organisationRepository.findById(dto.getOrganisationId()).get();
        User user = new User(dto.getEmail(), dto.getFirstName(), dto.getLastName(), organisation);
        if (this.userRepository.existsByEmail(dto.getEmail())) {
            log.info("User with email {} already exists", dto.getEmail());
            throw new EntityExistsException();
        }
        this.userRepository.save(user);
        log.info("Saved user with email {}", dto.getEmail());
        return user.getUserId();
    }

    @Override
    public boolean deleteUser(String email) {
        log.info("Deleting user {}", email);
        User user = this.userRepository.findByEmail(email).get();
        this.userRepository.delete(user);
        return true;
    }

    @Override
    public boolean registerToEvent(UserDTO dto, Long eventId) {
        Optional<Event> eventOptional = this.eventRepository.findById(eventId);
        Optional<User> user = this.userRepository.findByEmail(dto.getEmail());
        if (eventOptional.isEmpty() || user.isEmpty()) {
            throw new EntityNotFoundException();
        }
        Event event = eventOptional.get();
        event.getUsers().add(user.get());
        log.info("Registering user {} to event {}", user.get().getEmail(), event.getName());
        this.eventRepository.save(event);
        return true;
    }

    @Override
    public boolean unRegisterFromEvent(UserDTO dto, Long eventId) {
        Optional<Event> eventOptional = this.eventRepository.findById(eventId);
        if (eventOptional.isEmpty()) {
            throw new EntityNotFoundException();
        }
        Event event = eventOptional.get();
        log.info("UnRegistering  user {} from event {}", dto.getEmail(), event.getName());
        event.getUsers().remove(event);
        this.eventRepository.save(event);
        return true;
    }

    @Override
    public boolean addMeeting(User user, Meeting meeting) {
        log.info("Adding meeting for user {}", user.getEmail());
        user.getMeetings().add(meeting);
        this.userRepository.save(user);
        return true;
    }

    @Override
    public boolean addInvitation(User user, Invitation invitation) {
        log.info("Adding invitation for user {}", user.getEmail());
        user.getInvitations().add(invitation);
        this.userRepository.save(user);
        return true;
    }

    @Override
    public List<MeetingDTO> getUserMeetings(Long userId) throws EntityNotFoundException {

        Optional<User> user = this.userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new EntityNotFoundException();
        }
        return user.get().getMeetings().stream().map(MeetingDTO::new).collect(Collectors.toList());
    }

    @Override
    public List<InvitationDTO> getUserInvitations(Long userId) throws EntityNotFoundException {
        Optional<User> user = this.userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new EntityNotFoundException();
        }
        return user.get().getInvitations().stream().map(InvitationDTO::new).collect(Collectors.toList());
    }


}
