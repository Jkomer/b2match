package b2b.assignment.b2matchassignment.service.impl;

import b2b.assignment.b2matchassignment.model.Organisation;
import b2b.assignment.b2matchassignment.model.User;
import b2b.assignment.b2matchassignment.model.dto.OrganisationDTO;
import b2b.assignment.b2matchassignment.repository.OrganisationRepository;
import b2b.assignment.b2matchassignment.repository.UserRepository;
import b2b.assignment.b2matchassignment.service.OrganisationService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@Slf4j
public class OrganisationServiceImpl  implements OrganisationService {

    private OrganisationRepository repository;
    private UserRepository userRepository;

    @Autowired
    public OrganisationServiceImpl( OrganisationRepository repository, UserRepository userRepository){
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Override
    public Organisation createOrganisation(OrganisationDTO dto) throws EntityExistsException {
        String name = dto.getName();
        if(this.repository.existsByName(name)) {
            log.warn("Organisation with name {} already exists", name);
            throw new EntityExistsException();
        }
        Organisation organisation = new Organisation(name);
        this.repository.save(organisation);
        log.info("Successfully created Organisation with name: {}", name);
        return organisation;

    }

    @Override
    public Organisation getOrganisationByName(String name) {
        Optional<Organisation> organisation = this.repository.findByName(name);
        if(organisation.isEmpty()) {
            log.warn("And organisation with the name {} does not exist", name);
            return null;
        }
        return organisation.get();
    }


    @Override
    public void deleteOrganisation(OrganisationDTO dto) throws EntityNotFoundException {
        Organisation organisation = this.repository.findByName(dto.getName()).orElse(null);
        if(organisation == null) {
            throw new EntityNotFoundException();
        }
        organisation.setUsers(null);
        this.repository.save(organisation);
        this.repository.delete(organisation);

    }
}
