package b2b.assignment.b2matchassignment.service.impl;

import b2b.assignment.b2matchassignment.model.Constants;
import b2b.assignment.b2matchassignment.model.Invitation;
import b2b.assignment.b2matchassignment.model.Meeting;
import b2b.assignment.b2matchassignment.model.User;
import b2b.assignment.b2matchassignment.model.dto.InvitationDTO;
import b2b.assignment.b2matchassignment.repository.InvitationRepository;
import b2b.assignment.b2matchassignment.repository.MeetingRepository;
import b2b.assignment.b2matchassignment.repository.UserRepository;
import b2b.assignment.b2matchassignment.service.InvitationService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public class InvitationServiceImpl implements InvitationService {

    private UserRepository userRepository;
    private MeetingRepository meetingRepository;
    private InvitationRepository invitationRepository;
    private MeetingServiceImpl meetingService;

    @Autowired
    public InvitationServiceImpl(UserRepository userRepository,
                                 MeetingRepository meetingRepository,
                                 InvitationRepository invitationRepository,
                                 MeetingServiceImpl meetingService) {
        this.userRepository = userRepository;
        this.meetingRepository = meetingRepository;
        this.invitationRepository = invitationRepository;
        this.meetingService = meetingService;
    }

    @Override
    public Long sendInvitation(InvitationDTO dto, Long meetingId) {
        User user = this.userRepository.findByEmail(dto.getEmail()).get();
        Meeting meeting = this.meetingRepository.findById(meetingId).get();
        Invitation invitation = new Invitation(user, meeting);
        this.invitationRepository.save(invitation);
        return invitation.getInvitationId();
    }

    @Override
    public void handleInvitation(InvitationDTO dto, Long invitationId) {
        Invitation invitation = this.invitationRepository.findById(invitationId).get();
        invitation.setStatus(dto.getStatus());
        this.invitationRepository.save(invitation);
        Meeting meeting = this.meetingRepository.findById(invitation.getMeeting().getMeetingId()).get();
        updateMeetingStatus(meeting);
    }

    private void updateMeetingStatus(Meeting meeting) {
        List<String> statuses = meeting.getMeetingInvitations().stream().
                map(invitation -> invitation.getStatus())
                .collect(Collectors.toList());


        boolean meetingAccepted = true;

        for (String status : statuses) {
            if (status.equals(Constants.STATUS_ACCEPTED)) {
                continue;
            }
            meetingAccepted = false;
            break;
        }
        if (meetingAccepted) {
            this.meetingService.scheduleMeeting(meeting);
        } else if (statuses.contains(Constants.STATUS_REJECTED)) {
            this.meetingService.cancelMeeting(meeting);
        }


    }


}
