package b2b.assignment.b2matchassignment.service;

import b2b.assignment.b2matchassignment.model.dto.InvitationDTO;

public interface InvitationService {

    Long sendInvitation(InvitationDTO dto, Long meetingId);
    void handleInvitation(InvitationDTO dto,Long invitationId);
}
