package b2b.assignment.b2matchassignment.service;

import b2b.assignment.b2matchassignment.model.*;
import b2b.assignment.b2matchassignment.model.dto.InvitationDTO;
import b2b.assignment.b2matchassignment.model.dto.MeetingDTO;
import b2b.assignment.b2matchassignment.model.dto.UserDTO;

import java.util.List;

public interface UserService {

    public Long createUser(UserDTO dto);
    public boolean deleteUser(String email);
    public boolean registerToEvent(UserDTO dto, Long eventId);
    public boolean unRegisterFromEvent(UserDTO dto, Long eventId);
    public boolean addMeeting(User user, Meeting meeting);
    public boolean addInvitation(User user, Invitation invitation);
    public List<MeetingDTO> getUserMeetings(Long userId);
    public List<InvitationDTO> getUserInvitations(Long userId);
}
