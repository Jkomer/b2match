package b2b.assignment.b2matchassignment.service.impl;

import b2b.assignment.b2matchassignment.model.Event;
import b2b.assignment.b2matchassignment.repository.EventRepository;
import b2b.assignment.b2matchassignment.service.EventService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.sql.Date;
import java.util.List;

@Service
@NoArgsConstructor
@Slf4j
public class EventServiceImpl implements EventService {

    private EventRepository eventRepository;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public Long createEvent(String name, String eventDescription, Date startOfEvent, Date endOfEvent) {

        if(this.eventRepository.existsByName(name)) {
            log.warn("Event with this name already exists");
            throw new EntityExistsException();
        }
        Event event = new Event(name, eventDescription, startOfEvent, endOfEvent);
        this.eventRepository.save(event);
        log.info("Saved event  {}", name);
        return event.getEventId();

    }

    @Override
    public List<Event> findTopEvents(int top) {
        List<Event> events = this.eventRepository.findAll();
        if(events.size() < top) {
            return events;
        } else  {
            return events.subList(0, top);
        }
    }
}
