package b2b.assignment.b2matchassignment.service;

import b2b.assignment.b2matchassignment.model.Event;
import b2b.assignment.b2matchassignment.model.Meeting;
import b2b.assignment.b2matchassignment.model.dto.MeetingDTO;

import java.sql.Timestamp;

public interface MeetingService {

    Long createMeeting(MeetingDTO dto);
    boolean scheduleMeeting(Meeting meeting);
    boolean cancelMeeting(Meeting meeting);
    //boolean finishMeeting(Meeting meeting);
    //boolean startMeeting(Meeting meeting);
}
