package b2b.assignment.b2matchassignment.service.impl;

import b2b.assignment.b2matchassignment.model.Constants;
import b2b.assignment.b2matchassignment.model.Event;
import b2b.assignment.b2matchassignment.model.Meeting;
import b2b.assignment.b2matchassignment.model.User;
import b2b.assignment.b2matchassignment.model.dto.MeetingDTO;
import b2b.assignment.b2matchassignment.repository.EventRepository;
import b2b.assignment.b2matchassignment.repository.MeetingRepository;
import b2b.assignment.b2matchassignment.service.MeetingService;
import b2b.assignment.b2matchassignment.service.UserService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Service
@NoArgsConstructor
@Slf4j
public class MeetingServiceImpl implements MeetingService {

    private MeetingRepository meetingRepository;
    private UserService userService;
    private EventRepository eventRepository;

    @Autowired
    public MeetingServiceImpl(MeetingRepository meetingRepository, UserService userService, EventRepository eventRepository) {
        this.meetingRepository = meetingRepository;
        this.userService = userService;
        this.eventRepository = eventRepository;
    }

    @Override
    public Long createMeeting(MeetingDTO dto) {
        Event event = this.eventRepository.findById(dto.getEventId()).get();
        Meeting meeting = new Meeting(dto.getStartOfMeeting(),dto.getEndOfMeeting(),dto.getLocation(),
                Constants.STATUS_PENDING, event ,dto.getOrganiserEmail());
        this.meetingRepository.save(meeting);
        return meeting.getMeetingId();
    }



    @Override
    public boolean scheduleMeeting(Meeting meeting) {
        meeting.setStatus(Constants.STATUS_SCHEDULED);
        for (User invitee : meeting.getInvitees()) {
            this.userService.addMeeting(invitee, meeting);
        }
        this.meetingRepository.save(meeting);
        return true;
    }

    @Override
    public boolean cancelMeeting(Meeting meeting) {
        if (meeting.getStartOfMeeting().compareTo(LocalDateTime.now()) == -1) {
            if (meeting.getStatus().equals(Constants.STATUS_PENDING)) {
                meeting.setStatus(Constants.STATUS_CANCELED);
                this.meetingRepository.save(meeting);
            }
        }
        return true;
    }

  /*  @Override
    public boolean finishMeeting(Meeting meeting) {
        if(meeting.getEndOfMeeting().compareTo(LocalDateTime.now()) == -1
                && meeting.getStatus().equals(Constants.STATUS_ONGOING)) {
            meeting.setStatus(Constants.STATUS_FINISHED);
            this.meetingRepository.save(meeting);
        }
        return true;
    }*/

   /* @Override
    public boolean startMeeting(Meeting meeting) {
        LocalDateTime now = LocalDateTime.now();
        if(now.after(meeting.getStartOfMeeting()) && now.before(meeting.getEndOfMeeting())
                &)& meeting.getStatus().equals(Constants.STATUS_SCHEDULED)) {
            meeting.setStatus(Constants.STATUS_ONGOING);
            this.meetingRepository.save(meeting);
            meeting.getInvitees().forEach(invitee -> {
                invitee.getMeetings().remove(meeting);
            });
        }
        return true;
    }
*/

}
