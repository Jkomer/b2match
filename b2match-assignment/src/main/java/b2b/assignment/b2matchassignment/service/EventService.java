package b2b.assignment.b2matchassignment.service;

import b2b.assignment.b2matchassignment.model.Event;

import java.sql.Date;
import java.util.List;

public interface EventService {

    Long createEvent(String name,String eventDescription ,Date startOfEvent, Date endOfEvent);
    List<Event> findTopEvents(int top);

}
