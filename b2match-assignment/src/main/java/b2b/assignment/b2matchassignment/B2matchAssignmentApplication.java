package b2b.assignment.b2matchassignment;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class B2matchAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(B2matchAssignmentApplication.class, args);
	}

}
