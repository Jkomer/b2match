package b2b.assignment.b2matchassignment.b2match;

import b2b.assignment.b2matchassignment.model.Organisation;
import b2b.assignment.b2matchassignment.model.User;
import b2b.assignment.b2matchassignment.model.dto.UserDTO;
import b2b.assignment.b2matchassignment.repository.OrganisationRepository;
import b2b.assignment.b2matchassignment.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.PostConstruct;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class AssignmentTests {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    OrganisationRepository organisationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    void userCreationTest() throws Exception {
        Organisation organisation = new Organisation("TestOrg");
        organisationRepository.save(organisation);

        UserDTO dto = new UserDTO();
        dto.setEmail("email@email.com");
        dto.setFirstName("Marko");
        dto.setLastName("Juric");
        dto.setOrganisationId(organisation.getOrganisationId());
        mockMvc.perform(post("/api/v1/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk());
    }
}
